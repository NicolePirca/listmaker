package com.example.listmakerproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView



// aplicar el patron de diseño delegado ...adpatar sabe cuando se toca una lista un elemento
// no existe una conexion especifca en las clases bajo acoplamiento y la alta coexion se puede ocupara atributos de otras clases
// necesitamos saber algo de la adapter
// se creara una clase que va ha saber como interfaz si se esta tocando el item y este avise

interface ListSelectionListener {
    fun listItemsSelected(list: TaskList)
}

class ListRecyclerViewAdapte(
    private val lists: ArrayList<TaskList> = ArrayList(),
    val selectionListener: ListSelectionListener) :
    RecyclerView.Adapter <ListViewHolder>(){




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder, parent, false)
        return ListViewHolder(view)
        // se instancia esa vista para pasarle el view de la lista
         }

    override fun getItemCount(): Int {
        return lists.size

    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
    // este es el ultimo paso de nuestra imagen
        holder.listId.text= (position+1).toString()
        holder.listItemTitle.text= lists[position].name

        holder.itemView.setOnClickListener{ selectionListener.listItemsSelected(lists[position])}
    }

    fun addList(list: TaskList){
        lists.add(list)
        notifyItemInserted(lists.size -1)
        // para insertar para abajo
    }

}

