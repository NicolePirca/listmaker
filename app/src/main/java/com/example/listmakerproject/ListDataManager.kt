package com.example.listmakerproject

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager


class ListDataManager (private val context: Context){
    // guardar la lista para cuando se guarde la lista si se cierra la aplicacion
    val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun saveList(list: TaskList){
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit()
        sharedPreferences.putStringSet(list.name, list.tasks.toHashSet())
        sharedPreferences.apply()
        // funciona el share como un diccionario
        // guarda las cosas pero no mantienen el orden

    }
    fun readList(): ArrayList<TaskList>{
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val sharedPreferencesContext =sharedPreferences.all

        // para leer toda la lista de cosas
        val taskLists= ArrayList<TaskList>() // aqui van las cosas

        for (taskList in sharedPreferencesContext){ // esto sera la llave se trasforma en un arreglos esta lista
            val itemsHash= ArrayList(taskList.value as HashSet<String>)
            // para que funcione con un arreglo map
            val list = TaskList(taskList.key, itemsHash)

            taskLists.add(list)
        }
        return  taskLists
    }

    // Realizar

}

