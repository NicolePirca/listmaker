package com.example.listmakerproject

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    //esto es un contructor y llamandole en una misma linea de codigo
    val listId = itemView.findViewById<TextView>(R.id.item_id)
    val listItemTitle= itemView.findViewById<TextView>(R.id.listItemTitle)
    // titulo = item_idL
}
