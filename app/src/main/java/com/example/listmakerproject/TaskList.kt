package com.example.listmakerproject

import android.os.Parcel
import android.os.Parcelable

class TaskList (val name: String, val tasks: ArrayList<String> = ArrayList()): Parcelable{
    // costructos con sus atributos y si se le envia un task se inicializa un array en cero si no se le envia un arrays de tareas
// parcelaebel lo que en java es serializable
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "Generic List", //opcionales, puede o no tener un valor !! esto cuando esta estamos seguro que siempre va ha tener un valor
        parcel.createStringArrayList() ?: ArrayList()

    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeStringList(tasks)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskList> {
        override fun createFromParcel(parcel: Parcel): TaskList {
            return TaskList(parcel)
        }

        override fun newArray(size: Int): Array<TaskList?> {
            return arrayOfNulls(size)
        }
    }


}

