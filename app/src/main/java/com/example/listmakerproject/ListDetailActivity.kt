package com.example.listmakerproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListDetailActivity : AppCompatActivity() {

    lateinit var list: TaskList
    lateinit var  itemRecyclerView: RecyclerView
    lateinit var addItemButton: FloatingActionButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_detail)


      //  list= intent.getParcelableExtra(MainActivity,INTENT_LIST_KEY)
        // se extrae la lista que se envia
// list= intent.getParcelableExtra()
        title = list.name
    }
}
